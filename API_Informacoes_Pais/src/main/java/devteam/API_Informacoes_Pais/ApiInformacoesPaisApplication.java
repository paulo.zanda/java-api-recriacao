package devteam.API_Informacoes_Pais;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiInformacoesPaisApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiInformacoesPaisApplication.class, args);
	}

}
