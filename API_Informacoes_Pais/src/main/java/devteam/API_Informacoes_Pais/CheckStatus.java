package devteam.API_Informacoes_Pais;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckStatus {
    @GetMapping(path = "/api/check/status")
    public String check(){ return "online";}
}
