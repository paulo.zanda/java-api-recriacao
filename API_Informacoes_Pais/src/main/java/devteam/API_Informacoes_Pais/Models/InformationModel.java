package devteam.API_Informacoes_Pais.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "InformationModel")
public class InformationModel {
    
    @Id
    Integer identificador;
    
    @Column(nullable = false, length = 50)
    String nome, capital, regiao, subRegiao, area;
    
    public InformationModel(){
        this.identificador =0;
        this.nome = "";
        this.area = "";
        this.capital = "";
        this.regiao = "";
        this.subRegiao = "";
    }
    
    public InformationModel(Integer identificador, String nome, String area, String capital, String regiao, String subRegiao){
        this.identificador = identificador;
        this.nome = nome;
        this.regiao = regiao;
        this.area = area;
        this.subRegiao = subRegiao;
    }
    
    public void setIdentificador(Integer id){ this.identificador = id;}
    public Integer getIdentificador() {return identificador;}
    
    public void setNome(String nome){ this.nome = nome;}
    public String getNome (){return nome;}
    
    public void setCapital(String capital){ this.capital = capital;}
    public String getCapital(){ return capital;}

    public void setArea(String area) {this.area = area;}

    public String getRegiao() {return regiao;}
    public void setRegiao(String regiao) {this.regiao = regiao;}

    public String getSubRegiao() {
        return subRegiao;
    }

    public void setSubRegiao(String subRegiao) {
        this.subRegiao = subRegiao;
    }

    public String getArea() {
        return area;
    }

    @Override
    public String toString() {
        return "InformationModel{" +
                "identificador=" + identificador +
                ", nome='" + nome + '\'' +
                ", capital='" + capital + '\'' +
                ", regiao='" + regiao + '\'' +
                ", subRegiao='" + subRegiao + '\'' +
                ", area='" + area + '\'' +
                '}';
    }
}
