package devteam.api.rest.java.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //override -> isso quer dizer que esta classe é uma classe REST
public class StatusController {

    @GetMapping(path = "/api/status")
    /*
    *Esta notação é que diz para o JAVA e para o SPRING que este metodo sera executado quando houver uma requisição
    *   */
    public String check(){
        return "online";
    }

    //Esta classe foi criada apenas para retornar estado da nossa api
}
