package devteam.api.rest.java.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "user")
/*
* desta for eu digo ao JAVA que esta class é uma referencia de base dados.
* */
public class UserModel {

    @Id //desta forma eu digo ao JAVA que o cogido é um ID
    public Integer codigo;

    @Column(nullable = false, length = 50) //desta forma eu digo ao java que o nome é um campo.
    public String nome;

    @Column(nullable = false, length = 10) //desta forma eu digo ao java que o login é um campo.
    public String login;

    @Column(nullable = false, length = 10) //desta forma eu digo ao java que o senha é um campo.
    public String senha;

    public UserModel(){
        this.codigo =0;
        this.nome="";
        this.login="";
        this.senha="";
    }

    public UserModel(Integer codigo, String nome, String login, String senha){
        this.codigo = codigo;
        this.nome = nome;
        this.login = login;
        this.senha = senha;
    }

    public void setCodigo(Integer codigo){this.codigo = codigo;}
    public Integer getCodigo(){return codigo;}

    public void setNome(String nome){this.nome = nome;}
    public String getNome(){return nome;}

    public void setLogin(String login){this.login = login;}
    public String getLogin(){return login;}

    public void setSenha(String senha){this.senha = senha;}
    public String getSenha(){return senha;}

    @Override
    public String toString() {
        return "UserModel{" +
                "codigo =" + codigo +
                ", nome ='" + nome + '\'' +
                ", login ='" + login + '\'' +
                ", senha ='" + senha + '\'' +
                '}';
    }
}
