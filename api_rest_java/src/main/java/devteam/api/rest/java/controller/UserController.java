package devteam.api.rest.java.controller;

import devteam.api.rest.java.Model.UserModel;
import devteam.api.rest.java.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class UserController {
    /*
    * criando um user controller para verificar os usuarios apertir do codigo.
    *
    * PATHVARIABLE serve para acessar o codigo do GETMAPPING
    *
    * A CONEXAO COM A BASE DE DADO É FEITA NO application.properties
    * */

    @Autowired
    private UserRepository repository;

    @GetMapping(path = "/api/user/{codigo}")
    public ResponseEntity consultar (@PathVariable("codigo") Integer codito){
        return repository.findById(codito)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
        /*
        * GETMAPPING consulta o user pelo codigo e faz a consulta
        * */
    }

    /*
    * Metodo para salvar os dados trazidos na pesquisa
    * */
    @PostMapping (path = "/api/user/salvar") //recebe o metodo POST para envio de dados do browser para a base de dados.
    public UserModel salvar(@RequestBody UserModel user){
        return repository.save(user);

        /*o REQUESTBODY DIZ QUE NO CORPO DA REQUISICAO ELA POSSUI UM USER e os dados serao enviados no
        * corpo da requisicao*/
    }
}
