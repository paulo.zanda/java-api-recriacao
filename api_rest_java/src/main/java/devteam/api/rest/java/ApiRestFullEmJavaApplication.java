package devteam.api.rest.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//override projecto spring boot
@SpringBootApplication
public class ApiRestFullEmJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestFullEmJavaApplication.class, args);
	}

}
