package devteam.api.rest.java.repository;

import devteam.api.rest.java.Model.UserModel;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository <UserModel, Integer>{
/*
*  desta forma dizemos ao java que este classe é uma interface para gerir dados dos usuarios.
* */
}
